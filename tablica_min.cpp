//============================================================================
// Name        : tablica_min.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int solution (int *A, int size_of_A){
	int min = A[0];
	for(int i=0;i<size_of_A;i++){
		if(A[i]<min){
			min = A[i];
		}
	}return min;
}

int main() {

	int A[]={3, 42, 1, 12, 54, 32};

	cout<<solution(A, 6)<<endl;

	return 0;
}
