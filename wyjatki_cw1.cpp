//============================================================================
// Name        : wyjatki_cw1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <typeinfo>
#include <fstream>
using namespace std;

class Foo{
public:
	virtual ~Foo(){}
};

class Bar {
public:
	virtual ~Bar() {}
};

void displayException(exception& e){
	cout<<"exception: "<<e.what()<<endl;
}

void badException() throw (bad_exception){
	bad_exception e;
	throw e;
}

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	Bar b;
	Bar *c = 0;

	try{
//		int *test = new int[10000000000];//bad_alloc
//		int *test1 = new (nothrow) int[10000000000]; //nothrow powoduje, ze nie wyrzuci nam wyjatku chociaz powinno

//		Foo &f = dynamic_cast<Foo&>(b); bad cast

//		badException();

//		cout<<typeid(*c).name()<<endl;

//		ifstream f("moj plik nie istnieje");
//		f.exceptions(f.failbit);

		string text;
		text.at(2);


	}
	catch(bad_alloc& e){
		displayException(e);
	}
	catch(bad_cast& e){
		displayException(e);
	}
	catch(bad_exception& e){
		displayException(e);
	}
	catch(bad_typeid& e){
		displayException(e);
	}
	catch(ios_base::failure& e){
		displayException(e);
	}
	catch(out_of_range& e){
			displayException(e);
		}
	catch(...){
		cout<<"unrecognized exception"<<endl;
	}

	return 0;
}
