//============================================================================
// Name        : 14_03_1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

const int max_array_size=5;

int main() {

	int t[max_array_size];
	int liczba;
	int cyfra;
	int suma;

		cout << "Podaj 5 liczb" << endl;

		for(int i=0;i<max_array_size;++i)
			{
				cin >> t[i];
				liczba=t[i];
				suma=0;

				while(liczba>0)
					{
						cyfra=liczba%10;
						liczba/=10;
						suma+=cyfra;
					}
				if (suma%2==0)
					{
						cout << t[i] << endl;
					}
			}

		return 0;
}
