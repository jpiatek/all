//============================================================================
// Name        : 10_03_4.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

//obliczanie ciagu Fibonacciego
int Fibn(int i)
{
    if(i==0)
    	return 0;
    else if(i<3)
    	return 1;
    return Fibn(i-2)+Fibn(i-1);
}

int main() {
	int i;

	cout << "Podaj miejsce w ciagu ktore chcesz obliczyc: " << endl;
	cin >> i;
	Fibn(i);
	cout << Fibn(i) << endl;

	return 0;
}
