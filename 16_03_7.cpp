//============================================================================
// Name        : 16_03_7.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

// funkcje, na gorze musi byc deklaracja a cale jej dzialanie moze byc pozniej

int add(int a, int b);

int main() {
	int a =3;
	int b =4;

	cout << add(a,b) << endl;
	cout << add(add(a,4),b) << endl; // mozna uzyc funkcji w tej samej funkcji

	return 0;
}

int add(int a, int b){
	return a+b;
}
