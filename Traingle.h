/*
 * Traingle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef TRAINGLE_H_
#define TRAINGLE_H_
#include "Figure.h"


class Traingle: public Figure {
public:
	Traingle(std::string givenName, double na, double nh);
	double area();
	void draw();
	void getDescription();

private:
	double a;
	double h;

};

#endif /* TRAINGLE_H_ */
