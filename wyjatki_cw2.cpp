//============================================================================
// Name        : wyjatki_cw2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int *function(int n){
	int* tab = new int[n];
	return tab;
}

int *secondFunction(int n){
	int* tab = new (nothrow) int[n];
	return tab;
}



int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	int *firstArray;
	int *secondArray;
	int n,m;

	cout<<"podaj liczbe: "<<endl;
	cin>>n;
	cout<<"podaj druga liczbe: "<<endl;
	cin>>m;
	try{
		firstArray = function(n);
		secondArray = secondFunction(m);
		if(!secondArray){
			bad_alloc e;
			throw e;
		}
	}
	catch(bad_alloc &e){
		cout<<"bad_alloc: "<<endl;
	}
	catch(...){
		cout<<"unrecognized exception"<<endl;
	}
	return 0;
}
