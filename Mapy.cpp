//============================================================================
// Name        : Mapy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <map>
using namespace std;


int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	map<char, int> mymap;
	map<char,int>::iterator itlow, itup;

	mymap['a']=10;
	mymap['b']=20;
	mymap['c']=25;
	mymap['d']=30;
	mymap['e']=40;

	itlow=mymap.lower_bound('b');
	itup=mymap.upper_bound('d');

	mymap.erase(itlow, itup);

	for (map<char,int>::iterator it=mymap.begin(); it!=mymap.end(); ++it)
	    cout << it->first << " => " << it->second << '\n';

	return 0;
}
