//============================================================================
// Name        : wyjatki_cw3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <math.h>
#include <exception>
using namespace std;

class edomException : public exception{
	virtual const char* what() const throw(){
		return "EDOM exception";
	}
};

class erangeException : public exception{
	virtual const char* what() const throw(){
		return "ERANGE exception";
	}
};

double myPow(double x, double y){
	double z = pow(x,y);
	if(errno == EDOM){
		edomException e;
		throw e;
	}
	if (errno == ERANGE) {
		erangeException e;
		throw e;
	}
	return z;
}

double myLog(double x){
	double z = log(x);
	if (errno == EDOM) {
		edomException e;
		throw e;
	}
	if (errno == ERANGE) {
		erangeException e;
		throw e;
	}
	return z;
}


int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
//	int a = pow(3.0,1000000000);
//	if(errno == EDOM){
//		cout<<"EDOM"<<endl;
//	}
//	if(errno == ERANGE){
//		cout<<"ERANGE"<<endl;
//	}
//	cout<<"a: "<<a<<endl;
//	return 0;
	try{
		int x = myPow(10000, 2);
		int y = myLog(-1);
	}
	catch(exception& e){
		cout<<"exception: "<<e.what()<<endl;
	}
}
