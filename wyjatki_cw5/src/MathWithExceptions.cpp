/*
 * MathWithExceptions.cpp
 *
 *  Created on: 18.05.2017
 *      Author: RENT
 */

#include "MathWithExceptions.h"

namespace math{
using namespace std;

MathWithExceptions::MathWithExceptions() {
	// TODO Auto-generated constructor stub

}

MathWithExceptions::~MathWithExceptions() {
	// TODO Auto-generated destructor stub
}

double MathWithExceptions::log(double x){
	double z = ::log(x);
	if (errno == EDOM) {
		DomainError e;
		throw e;
	}
	if (errno == ERANGE) {
		RangeError e;
		throw e;
	}
	return z;
}
double MathWithExceptions::pow(double x, double y){
	double z = ::pow(x,y);
	if (errno == EDOM) {
		DomainError e;
		throw e;
	}
	if (errno == ERANGE) {
		RangeError e;
		throw e;
	}
	return z;
}
double MathWithExceptions::cos(double x){
	double z = ::cos(x);
	if (errno == EDOM) {
		DomainError e;
		throw e;
	}
	if (errno == ERANGE) {
		RangeError e;
		throw e;
	}
	return z;
}
double MathWithExceptions::sin(double x){
	double z = ::sin(x);
	if (errno == EDOM) {
		DomainError e;
		throw e;
	}
	if (errno == ERANGE) {
		RangeError e;
		throw e;
	}
	return z;
}
double MathWithExceptions::tan(double x){
	double z = ::tan(x);
	if (errno == EDOM) {
		DomainError e;
		throw e;
	}
	if (errno == ERANGE) {
		RangeError e;
		throw e;
	}
	return z;
}

}
