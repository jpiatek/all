/*
 * MathWithExceptions.h
 *
 *  Created on: 18.05.2017
 *      Author: RENT
 */

#ifndef MATHWITHEXCEPTIONS_H_
#define MATHWITHEXCEPTIONS_H_
#include <math.h>
#include <exception>
#include <iostream>


namespace math{
using namespace std;

class MathWithExceptions {
public:
	class DomainError : public exception{
	public:
	virtual const char* what() const throw(){
				return "Domain error";
			}
	};
	class RangeError : public exception{
	public:
		virtual const char* what() const throw () {
				return "Range error";
			}
	};

MathWithExceptions();
virtual ~MathWithExceptions();

static double log(double x);
static double pow(double x, double y);
static double cos(double x);
static double sin(double x);
static double tan(double x);

};
}
#endif /* MATHWITHEXCEPTIONS_H_ */
