//============================================================================
// Name        : 21_03_3Fib.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int fib(int n){
	if (n==0)
		return 0;
	if (n==1)
		return 1;
	else
		return fib(n-1) + fib(n-2);
}

int main() {

int n;

cout << "Podaj ktory element ciagu Fibonacciego mam wyswietlic " << endl;
cin >> n;

cout << "miejsce " << n << " w ciagu Fib to: " << fib(n) ;

	return 0;
}
