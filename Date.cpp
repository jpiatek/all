
#include "Date.h"
#include <iostream>
using namespace std;

using namespace std;

Date::Date():day(2), month(3), year(1904){
	}
Date::Date(int nday, int nmonth, int nyear){
	day=nday;
	month=nmonth;
	year=nyear;
}
bool Date::operator==(const Date& other)const{

	if(day == other.day && month == other.month && year == other.year){
		return true;
	}
	else
		return false;
}
bool Date::operator!=(const Date& other)const{
		return !operator==(other);
	}
bool Date::operator>=(const Date& other)const{

	if(year > other.year){
		return true;
	}
	else if(year == other.year){
		if (month>other.month){
			return true;
		}
			else if (month == other.month){
				if (day > other.day){
					return true;
				}
			}
	}
		return false;
}

int Date::getDay()const{
	return day;
}
void Date::setDay(int nday){
	day=nday;
}
int Date::getMonth()const{
	return month;
}
void Date::setMonth(int nmonth){
	month=nmonth;
}
int Date::getYear()const{
	return year;
}
void Date::setYear(int nyear){
	year=nyear;
}
void Date::printDate()const{
	cout << day <<" "<< month <<" "<< year << endl;
}

bool Date::isValid()const{

	switch (day)
		{
		case 1:
			if(day<=30 && (month==2 || month==4 || month==6 || month==9 || month==11))
			{
				return 1;
			}
		case 2:
			if(day<=31 && (month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12))
			{
				return 1;
			}
		case 3:
			if(day<=29 && month==2 && (year%4==0 && year%100!=0) || year%400==0)
			{
				return 1;
			}
		default:
			return 0;
		break;
		}
}
bool Date::isInLappYear()const{
	if((year%4==0 && year%100!=0) || year%400==0)
		{
		return 1;
		}
	else
		return 0;
}
int Date::daysTillTheEndOfTheMonth()const{
	int suma;
	int t1[12]={31,28,31,30,31,30,31,31,30,31,30,31};
	int t2[12]={31,29,31,30,31,30,31,31,30,31,30,31};

	if(isInLappYear()){
		suma=t2[month-1]-day;
	}
	else{
		suma=t1[month-1]-day;
	}
	return suma;
}
int Date::daysTillTheEndOfTheYear()const{
	int suma;
	int t1[12]={31,28,31,30,31,30,31,31,30,31,30,31};
	int t2[12]={31,29,31,30,31,30,31,31,30,31,30,31};

	if(isInLappYear()){
			suma=t2[month-1]-day;
			for (int i=month; i<12; ++i)
			suma+=t2[i];
		}
			else{
			suma=t1[month-1]-day;
			for (int i=month; i<12; ++i)
			suma+=t1[i];
		}
		return suma;
}

