//============================================================================
// Name        : wyjatki_cw4.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <exception>
using namespace std;

class MyArray {
public:
	class memoryException : public exception{
	public:
		virtual const char* what() const throw(){
			return "memory exception";
		}
	};

	MyArray(int size = 0){
		array_size = size;
		if(array_size == 0){
			return;
		}
		if(array_size <0){
			memoryException e;
			throw e;
		}
		try{
			array = new int[array_size];
		}
		catch(bad_alloc&){
			memoryException e;
			throw e;
		}

	}
	~MyArray(){
		if(array_size>0){
			delete[] array;
		}
	}

	int at(int posiotion) {
		if(posiotion>=array_size){
			memoryException e;
			throw e;
		}
		return array[posiotion];
	}
	void setAt(int posiotion, int value) {
		if (posiotion >= array_size) {
			memoryException e;
			throw e;
		}
		array[posiotion] = value;
	}

	void resize(int new_size) {
		int *old_array = array;
		array = new int[new_size];
		try{
			array = new int[new_size];
		}
		catch(bad_alloc&){
			cout<<"bad alloc catched"<<endl;
			delete[] old_array;
			array_size = 0;
			memoryException e;
			throw e;
		}
		for(int i=0;i<array_size;i++){
			array[i] = old_array[i];
		}
		delete[] old_array;
		array_size=new_size;
	}
	void append(int element_to_add) {

	}

	int array_size;
	int *array;

};

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	try{
		MyArray test(2);
		test.setAt(0,5);
		test.resize(-2);
	}
	catch(MyArray::memoryException& e){
		cout<<"MyArray error occured, going to terminate: "<<e.what()<<endl;
	}
	catch(exception& e){
		cout<<"error occured, going to terminate: "<<e.what()<<endl;
	}

	return 0;
}
