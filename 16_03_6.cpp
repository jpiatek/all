//============================================================================
// Name        : 16_03_6.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

const int max_size=5;

int main() {

	int t[max_size]={};
	int liczba;
	int miejsce;

	cout << "Podaj liczbe, ktora chcesz zapisac: " << endl;
	cin >> liczba;
	cout << "podaj miejsce od 0 do 4 gdzie chcesz zapisac? " << endl;
	cin >> miejsce;

	int *it=&t[miejsce];

	*it=liczba;

	for (int i=0; i<max_size; ++i){
			cout << t[i] << " ";
		}

	return 0;
}
