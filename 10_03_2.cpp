//============================================================================
// Name        : 10_03_2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

//piramida liczb do podanej wartosci
int main() {

	int liczba;

	cout << "Podaj liczbe" << endl;
	cin >> liczba;

	for(int i=1; i<=liczba; ++i)
	{
		for(int j=i; j>=1; j-=1)
				{
					cout << j << flush;
				}
		cout << endl;
	}

	for(int i=liczba; i>=1; --i)
	{
		for(int j=i-1; j>=1; j-=1)
				{
					cout << j << flush;
				}
		cout << endl;
	}

	return 0;
}
