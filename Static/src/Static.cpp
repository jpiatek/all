//============================================================================
// Name        : Static.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "B.h"

int b=0;

void f(){
	static int s=0;
	int a =0;
	cout << s++ << " " << ++a << endl;
	cout << b << endl;
}

int main() {

//	for(int i=0;i<5;i++){
//		f();
//		b+=2;
//	}

B new_class;
cout<<new_class.getR()<<endl;
new_class.setR(8);
cout<<new_class.getR()<<endl;

B a;
B b;
B c;

cout<<c.getCreatedObjects()<<endl;
cout<<c.getAliveOjbects()<<endl;
cout<<B::getAliveOjbects()<<endl;
	return 0;
}
