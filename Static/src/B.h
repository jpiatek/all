/*
 * B.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef B_H_
#define B_H_

class B {
public:
	B();
	B(const B& object);
	virtual ~B();

	static int getR(){
		return r;
	}

	void setR(int r) {
		this->r = r;
	}

	static int getCreatedObjects(){
		return createdObjects;
	}

	static int getAliveOjbects(){
		return aliveOjbects;
	}

private:
	static int r;
	static int createdObjects;
	static int aliveOjbects;
};

#endif /* B_H_ */
