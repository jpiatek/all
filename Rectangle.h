/*
 * Rectangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include "Figure.h"

class Rectangle: public Figure {
public:
	Rectangle(std::string givenName, double na, double nb);

	double area();
	void draw();
	void getDescription();

private:
	double a;
	double b;
};

#endif /* RECTANGLE_H_ */
