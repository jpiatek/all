/*
 * Array.h
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef ARRAY_H_
#define ARRAY_H_

const int MAX=1000;

class Array {
public:
	Array();
	virtual ~Array();

	int getCurrentSize() const {
		return currentSize;
	}
	void setCurrentSize(int currentSize) {
		this->currentSize = currentSize;
	}
	const int* getTab() const {
		return tab;
	}

	int getElement(int index);
	void addAtEnd(int element);
	void removeFromEnd();
	void addAtSpecificIndex(int index, int element);
	void removeFromSpecificIndex(int index);
	void fillInArrayWithSomething(int limit, int number);
	int getLastValue();
	int getSpecificValue(int index);
	int getMaxValue();
	int getMinValue();


private:
	int currentSize;
	int tab[MAX]={};
};

#endif /* ARRAY_H_ */
