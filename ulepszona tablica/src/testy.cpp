/*
 * testy.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */
#include "gtest.h"
#include "Array.h"

class TestingArray : public testing::Test{
protected:
	Array array;
	Array emptyArray;
	virtual void SetUp()
	{
		array.fillInArrayWithSomething(10,2);
	}
	virtual void TearDown()
	{

	}
};

TEST_F(TestingArray, currentSize){
	EXPECT_EQ(10, array.getCurrentSize());
}
TEST_F(TestingArray, wrongCurrentSize){
	EXPECT_NE(40, array.getCurrentSize());
}
TEST_F(TestingArray, specificElement){
	EXPECT_EQ(2, array.getElement(3));
	EXPECT_EQ(-1, array.getElement(12));
}
TEST_F(TestingArray, addingAtEnd){
	array.addAtEnd(3);
	EXPECT_EQ(3, array.getLastValue());
}
TEST_F(TestingArray, addingAtEndInEmptyArray){
	emptyArray.addAtEnd(4);
	EXPECT_EQ(4, emptyArray.getLastValue());
}
TEST_F(TestingArray, removingLastElement){
	array.addAtEnd(4);
	EXPECT_EQ(4, array.getLastValue());
	array.removeFromEnd();
	EXPECT_EQ(2, array.getLastValue());
}
TEST_F(TestingArray, removingLastElementFromEmptyArray){
	emptyArray.removeFromEnd();
	EXPECT_EQ(0, emptyArray.getCurrentSize());
}
TEST_F(TestingArray, addingAtSpecificIndex){
	array.addAtSpecificIndex(3, 3);
	EXPECT_EQ(3, array.getSpecificValue(3));
}
TEST_F(TestingArray, addingAtSpecificIndexInEmptyArray){
	emptyArray.addAtSpecificIndex(4, 3);
	EXPECT_EQ(3, emptyArray.getSpecificValue(4));
	EXPECT_EQ(0, emptyArray.getSpecificValue(20));
}
TEST_F(TestingArray, addingAtIndexHigherThanArray){
	array.addAtSpecificIndex(14, 4);
	EXPECT_EQ(4, array.getSpecificValue(14));
	EXPECT_EQ(0, array.getSpecificValue(13));
}
TEST_F(TestingArray, removingFromSpexificArray){
	array.removeFromSpecificIndex(5);
	EXPECT_EQ(0, array.getSpecificValue(11));
}
TEST_F(TestingArray, getMaxValue){
	emptyArray.addAtEnd(4);
	emptyArray.addAtEnd(7);
	EXPECT_EQ(7, emptyArray.getMaxValue());
}
