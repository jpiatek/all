/*
 * Array.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "Array.h"

Array::Array() {
	currentSize=0;
}
Array::~Array() {

}

int Array::getElement(int index){
	if(index>=0 && index<currentSize){
		return tab[index];
	}
	return -1;
}

void Array::addAtEnd(int element){
	if (currentSize<MAX){
		tab[currentSize]=element;
		currentSize++;
	}
}

void Array::removeFromEnd(){
	if(currentSize>0){
	tab[currentSize]=0;
	currentSize--;
	}
}

void Array::addAtSpecificIndex(int index, int element){
	if(index>=0 && index<MAX){
		int temp;
		for(int i=index; i<=currentSize; ++i){
			temp=tab[i];
			tab[i+1]=temp;
		}
	tab[index]=element;
	currentSize++;
	}
}

void Array::removeFromSpecificIndex(int index){
	if(index>=0 && index<=MAX){
			int temp;
			for(int i=index + 1; i<currentSize; ++i){
				temp=tab[i];
				tab[i-1]=temp;
			}
			currentSize--;
		}
}

void Array::fillInArrayWithSomething(int limit, int number){

	if(limit<MAX){
	for(int i=0;i<limit;i++){
		tab[i]=number;
	}
	currentSize=limit;
	}
}

int Array::getLastValue(){
	return tab[currentSize-1];
}

int Array::getSpecificValue(int index){
	return tab[index];
}

int Array::getMaxValue(){
	int max;
	max=tab[0];
	for(int i=0; i<currentSize; i++){
		if(tab[i]>max){
			max=tab[i];
		}
	}return max;
}
int Array::getMinValue(){
	int min;
	min=tab[0];
	for(int i=0; i<currentSize; i++){
		if(tab[i]<min){
			min=tab[i];
		}
	}return min;
}
