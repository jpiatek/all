//============================================================================
// Name        : 10_03_6.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
using namespace std;

int main() {

	int pole;
	int objetosc;
	int wybor;

	cout << "Wybierz figure, ktorej pole i objetosc chcesz obliczyc: " << endl;
	cout << "1. szescian " << endl;
	cout << "2. prostopadloscian " << endl;
	cout << "3. kula " << endl;
	cout << "4. ostroslup " << endl;
	cout << "5. walec " << endl;

	cin >> wybor;

	switch(wybor)
	{
	case 1:
		int a;
		cout << "podaj dlugosc krawedzi" << endl;
		cin >> a;
		pole=(a*a)*6;
		objetosc=a*a*a;

		cout << "pole szescianu to " << pole << endl;
		cout << "objetosc szescianu to " << objetosc << endl;
		break;
	case 2:
		int c;
		int b;
		int h;
		cout << "podaj dl krawedzi podstawy (a i b) " << endl;
		cin >> c >> b;
		cout << "podaj wysokosc " << endl;
		cin >> h;

		pole=(2*c*b)+(2*b*h)+(2*c*h);
		objetosc=c*b*h;

		cout << "pole prostopadloscianu to: " << pole << endl;
		cout << "objetosc prostopadloscianu to: " << objetosc << endl;
		break;
	case 3:
		int d;
		cout << "podaj promien " << endl;
		cin >> d;

		pole=4*M_PI*(d*d);
		objetosc=((3*M_PI)/4)*(d*d*d);

		cout << "pole kuli to: " << pole << endl;
		cout << "objetosc kuli to: " << objetosc << endl;
		break;
	case 4:
		int g;
		int j;
		int i;

		cout << "Podaj pole podstawy ostroslupa " << endl;
		cin >> g;
		cout << "Podj pole powierzchni bocznej " << endl;
		cin >> j;
		cout << "Podj wysokosc ostroslupa " << endl;
		cin >> i;


		pole=g+j;
		objetosc=(g/3)*i;

		cout << "pole ostroslupa to: " << pole << endl;
		cout << "objetosc ostroslupa to: " << objetosc << endl;
		break;
	case 5:
		int e;
		int f;
		cout << "podaj wysokosc walca " << endl;
		cin >> e;
		cout << "podaj promien podstawy walca " << endl;
		cin >> f;

		pole=(2*M_PI*(f*f))+(2*M_PI*f*e);
		objetosc=M_PI*(f*f)*e;

		cout << "pole walca to: " << pole << endl;
		cout << "objetosc walca to: " << objetosc << endl;
		break;

	default:
			cout << "brak komendy, wybierz cyfre od 1 do 5" << endl;
	}

	return 0;
}
