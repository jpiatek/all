/*
 * Rectangle.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Rectangle.h"
#include <iostream>

Rectangle::Rectangle(std::string givenName, double na, double nb):
Figure(givenName){
	a=na;
	b=nb;
}

double Rectangle::area(){
	return a*b;

}
void Rectangle::draw(){
	for(int i=0;i<a;++i){
		for(int j=0;j<b;++j){
			std::cout<< " * ";
		}
		std::cout<<std::endl;
	}
}
void Rectangle::getDescription(){
	std::cout<< getName() << std::endl;
	std::cout<< "jeden bok ma: " << a << " drugi ma: " << b << std::endl;
	std::cout<< "pole wynosi: " << area() << std::endl;
	draw();
}
