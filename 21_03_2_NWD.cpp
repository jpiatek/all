//============================================================================
// Name        : 21_03_2_NWD.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

//nwd z wykorzystaniem rekurencji

int nwd(int k, int n){
	if(k==0)
		return n;
	else
		return nwd(n%k,k);
}

int main() {
	int k;
	int n;

	cout << "podaj dwie liczby: " << endl;
	cin >> k >> n;

	cout << "NWD dla tych liczb to: " << nwd(k,n) << endl;

	return 0;
}
