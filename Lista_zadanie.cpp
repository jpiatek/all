//============================================================================
// Name        : Lista_zadanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
using namespace std;

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	list<int>::iterator it, it2, it3;
	int myints[]={1,2,3,4,5,6,7,8,9};
	list<int>first(myints, myints+9);

	int myints2[]={9,8,7,6,5,4,3,2,1};
	list<int>second(myints2, myints2+9);

	it = first.begin();
	advance(it, 2);

	it2 = second.begin();
	advance(it2, 2);

	it3= second.begin();
	advance(it3, 7);

	first.splice(it, second, it2, it3);


	cout<<"First contains: "<<endl;
	for (list<int>::iterator it=first.begin(); it!=first.end(); ++it)
	    cout << ' ' << *it;
	  cout << '\n';

	  cout<<"Second contains: "<<endl;
	  for (list<int>::iterator it=second.begin(); it!=second.end(); ++it)
		  cout << ' ' << *it;
	  cout << '\n';

	return 0;
}
