// constructing vectors
#include <iostream>
#include <vector>

int main ()
{

	std::vector<int>myvector(10);

	for(unsigned int i=0; i<myvector.size(); i++){
		myvector.at(i)=i;
	}

	for(unsigned int i=0; i<myvector.size(); i++){
		std::cout<<" "<<myvector.at(i);
	}
	std::cout<<"\n";

	std::cout<<myvector[15]<<std::endl;
	std::cout<<myvector.at(5)<<std::endl;
  return 0;
}
