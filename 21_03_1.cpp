//============================================================================
// Name        : 21_03_1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

// rekurencja czyli funkcja wywoluje sama siebie

int silnia(int n)
{
	if (n==0)
		return 1;
	else
		return n*silnia(n-1);
}

int main() {
int n;

cout << "podaj liczbe" << endl;
cin >> n;

cout << "silnia to: " << silnia(n) << endl;

	return 0;
}
