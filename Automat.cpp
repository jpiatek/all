/*
 * Automat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Automat.h"
#include <string>

Automat::Automat() {
	listOfProducts [0]= {"Empty", 0, 0};
	listOfProducts [1]= {"Latte", 500, 11};
	listOfProducts [2]= {"Espresso", 400, 11};
	listOfProducts [3]= {"Cappucino", 600, 11};
	listOfProducts [4]= {"Frappe", 700, 11};
	listOfProducts [5]= {"Americana", 800, 11};
	givenMoney=0;
	coinsLimit=0;


}

Automat::~Automat() {

}

int Automat::showPrice(int index){
	if(index>0 && index<=maxSize && listOfProducts[index].limit!=0){
		choosenProduct = listOfProducts[index];
		return choosenProduct.price;
	}
		return listOfProducts[0].price;
}
int Automat::putMoney(int amount){
	if(coinsLimit<=maxCoinAmount){
		if (choosenProduct.price>amount){
			givenMoney+=amount;
			coinsLimit++;
			return amount-choosenProduct.price;
		}
		if (choosenProduct.price==amount){
			givenMoney+=amount;
			choosenProduct.limit--;
			coinsLimit++;
			return 0;
		}
		if (choosenProduct.price<amount){
			givenMoney=+amount;
			choosenProduct.limit--;
			coinsLimit++;
			return -(choosenProduct.price-amount);
		}
		return 0;
	}
	return -1;
}

int Automat::cancelPurchase(){
	if(givenMoney!=0){
		choosenProduct=listOfProducts[0];
		choosenProduct.limit++;
		coinsLimit=0;
		return givenMoney;
	}
	return 0;
}


