/*
 * Wheel.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef WHEEL_H_
#define WHEEL_H_

#include "Figure.h"

class Wheel: public Figure {
public:
	Wheel(std::string givenName, double nr);
	double area();
	void draw();
	void getDescription();

private:
	double r;

};

#endif /* WHEEL_H_ */
