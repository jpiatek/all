/*
 * Square.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef SQUARE_H_
#define SQUARE_H_

#include "Figure.h"

class Square: public Figure {
public:
	Square(std::string givenName, double na);
	double area();
	void draw();
	void getDescription();

private:
	double a;
};

#endif /* SQUARE_H_ */
