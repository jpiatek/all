/*
 * DriveSystem.cpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#include "DriveSystem.h"

DriveSystem::DriveSystem() {
	frontShift = 1;
	rearShift = 1;
	frontMaxShift = 3;
	rearMaxShift = 6;
}

DriveSystem::DriveSystem(int frontMaxShift, int rearMaxShift) {
	frontShift = 1;
	rearShift = 1;
	this->frontMaxShift = frontMaxShift;
	this->rearMaxShift = rearMaxShift;
}

void DriveSystem::shiftFrontGearUp() {
	if (frontShift < frontMaxShift)
		frontShift++;
}
void DriveSystem::shiftFrontGearDown() {
	if (frontShift > 1)
		frontShift--;
}
void DriveSystem::shiftRearGearUp() {
	if (rearShift < rearMaxShift)
		rearShift++;
}
void DriveSystem::shiftRearGearDown() {
	if (rearShift > 1)
		rearShift--;
}
