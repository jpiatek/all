/*
 * Bike.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef BIKE_H_
#define BIKE_H_
#include "Wheel.h"
#include "Frame.h"
#include "Lights.h"
#include "DriveSystem.h"

class Bike {
private:
	Frame frame;
	Wheel wheels;
	Lights lights;
	DriveSystem driveSystem;

	int speed;
	int direction;

public:
	Bike();
	Bike(int wheelSize, int maxFrontShift, int maxRearShift, Frame::frameType type, Frame::frameSize size, Frame::frameMaterial material);
	Bike(int speed, int direction, int wheelSize, int maxFrontShift, int maxRearShift, Frame::frameType type, Frame::frameSize size, Frame::frameMaterial material);

	int getSpeed() const {return speed;};
	int getDirection() const {return direction;};

	void setDirection(int direction) {this->direction = direction;};
	void setSpeed(int speed) {this->speed = speed;};

	void accelerate();
	void breaking();
	void turn(int deegre);

	void discribe();

	void turnLightsOn(std::string light);
	void turnLightsOff(std::string light);

	void shiftFrontGear(std::string direction);
	void shiftRearGear(std::string direction);

};

#endif /* BIKE_H_ */

// rower:
//	koloa 		klasa
//	rama		klasa
//	hamules
//	amortyzator
//	kierownica
//	siodelko
//	oswietlenie (przod tyl odblaski) 	klasa
//	uklad napedowy						klasa
//		lancuch
//		biegi przod
//		biegi tyl
//		przerzutka
//		pedal
//
//metody
//	jedz
//	hamuj
//	wlacz swiatla
//	skrec
//	zmien bieg
//
