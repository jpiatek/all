//============================================================================
// Name        : klasyRower.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Bike.h"

int main() {

	Bike bike1;

	bike1.discribe();

	bike1.turnLightsOn("front");
	bike1.setSpeed(10);
	bike1.setDirection(45);
	bike1.shiftFrontGear("up");
	bike1.shiftRearGear("up");
	bike1.discribe();


	Bike coustomBike(22,1,4,Frame::hardTail,Frame::XL,Frame::aluminium);
	coustomBike.discribe();

	Bike coustomBike2(5, 90, 22,1,4,Frame::hardTail,Frame::XL,Frame::aluminium);
	coustomBike2.discribe();

	return 0;
}
