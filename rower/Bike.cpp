/*
 * Bike.cpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "Bike.h"

Bike::Bike() {
	speed = 0;
	direction = 0;
}
Bike::Bike(int wheelSize, int maxFrontShift, int maxRearShift, Frame::frameType type, Frame::frameSize size, Frame::frameMaterial material){
	speed = 0;
	direction = 0;

	wheels.setFrontWheelSize(wheelSize);
	wheels.setRearWheelSize(wheelSize);
	driveSystem.setFrontMaxShift(maxFrontShift);
	driveSystem.setRearMaxShift(maxRearShift);
	frame.setFrameType(type);
	frame.setFrameSize(size);
	frame.setFrameMaterial(material);
}

Bike::Bike(int speed, int direction, int wheelSize, int maxFrontShift, int maxRearShift, Frame::frameType type, Frame::frameSize size, Frame::frameMaterial material)
:speed(speed), direction(direction), wheels(wheelSize),driveSystem(maxFrontShift,maxRearShift),frame(type,size,material){}


void Bike::discribe() {
	std::cout << "YOUR BIKE\n";
	std::cout << "=========\n";
	std::cout << "Frame type > " << frame.getFrameType() << std::endl;
	std::cout << "Frame size > " << frame.getFrameSize() << std::endl;
	std::cout << "Frame material > " << frame.getFrameMaterial() << std::endl;
	std::cout << "Front wheel size > " << wheels.getFrontWheelSize()
			<< std::endl;
	std::cout << "Rear wheel size > " << wheels.getRearWheelSize() << std::endl;
	std::cout << "Front light is on > " << lights.isFrontLightOn() << std::endl;
	std::cout << "Rear light is on > " << lights.isRearLightOn() << std::endl;
	std::cout << "Front shift > " << driveSystem.getFrontShift() << std::endl;
	std::cout << "Max front shift > " << driveSystem.getFrontMaxShift()
			<< std::endl;
	std::cout << "Rear shift > " << driveSystem.getRearShift() << std::endl;
	std::cout << "Max rear shift > " << driveSystem.getRearMaxShift()
			<< std::endl;
	std::cout << "Speed > " << getSpeed() << std::endl;
	std::cout << "Direction > " << getDirection() << std::endl << std::endl;
}

void Bike::turnLightsOn(std::string light) {
	if (light == "front") {
		lights.frontLightOn();
	} else if (light == "rear") {
		lights.rearLightOn();
	} else {
		std::cout << "Error input > " << light << std::endl;
	}
}
void Bike::turnLightsOff(std::string light) {
	if (light == "front") {
		lights.frontLightOff();
	} else if (light == "rear") {
		lights.rearLightOff();
	} else {
		std::cout << std::endl << "**************" << std::endl;
		std::cout << "Error input > " << light << std::endl;
		std::cout << "**************" << std::endl << std::endl;
	}
}

void Bike::shiftFrontGear(std::string direction) {
	if (direction == "up") {
		driveSystem.shiftFrontGearUp();
	} else if (direction == "down") {
		driveSystem.shiftFrontGearDown();
	} else {
		std::cout << std::endl << "**************" << std::endl;
		std::cout << "Error input > " << direction << std::endl;
		std::cout << "**************" << std::endl << std::endl;
	}
}

void Bike::shiftRearGear(std::string direction) {
	if (direction == "up") {
		driveSystem.shiftRearGearUp();
	} else if (direction == "down") {
		driveSystem.shiftRearGearDown();
	} else {
		std::cout << std::endl << "**************" << std::endl;
		std::cout << "Error input > " << direction << std::endl;
		std::cout << "**************" << std::endl << std::endl;
	}
}
