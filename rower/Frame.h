/*
 * Frame.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef FRAME_H_
#define FRAME_H_

class Frame {

public:
	enum frameType{hardTail, softTail};
	enum frameSize{S,M,L,XL};
	enum frameMaterial{steel, aluminium, carbon};

	Frame();
	Frame(frameType type, frameSize size, frameMaterial material);

	frameType getFrameType() const {return type;};
	frameSize getFrameSize() const {return size;};
	frameMaterial getFrameMaterial() const {return material;};

	void setFrameType(frameType type){this->type = type;};
	void setFrameSize(frameSize size){this->size = size;};
	void setFrameMaterial(frameMaterial material){this->material = material;};

private:
	frameType type;
	frameSize size;
	frameMaterial material;

};

#endif /* FRAME_H_ */
