/*
 * DriveSystem.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef DRIVESYSTEM_H_
#define DRIVESYSTEM_H_

class DriveSystem {
private:
	int frontShift;
	int rearShift;
	int frontMaxShift;
	int rearMaxShift;
public:
	DriveSystem();
	DriveSystem(int frontMaxShift, int rearMaxShift);

	void shiftFrontGearUp();
	void shiftFrontGearDown();
	void shiftRearGearUp();
	void shiftRearGearDown();

	int getFrontMaxShift() const {return frontMaxShift;};
	int getFrontShift() const {return frontShift;};
	int getRearMaxShift() const {return rearMaxShift;};
	int getRearShift() const {return rearShift;}

	void setFrontMaxShift(int frontMaxShift) {this->frontMaxShift = frontMaxShift;}
	void setFrontShift(int frontShift) {this->frontShift = frontShift;}
	void setRearMaxShift(int rearMaxShift) {this->rearMaxShift = rearMaxShift;}
	void setRearShift(int rearShift) {this->rearShift = rearShift;}


};

#endif /* DRIVESYSTEM_H_ */
