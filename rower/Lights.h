/*
 * Lights.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef LIGHTS_H_
#define LIGHTS_H_

class Lights {
private:
	bool frontLightIsOn;
	bool rearLightIsOn;
public:
	Lights();
	void frontLightOn(){frontLightIsOn = true;};
	void frontLightOff(){frontLightIsOn = false;};
	void rearLightOn(){rearLightIsOn = true;};
	void rearLightOff(){rearLightIsOn = false;};

	bool isFrontLightOn() const {return frontLightIsOn;};
	bool isRearLightOn() const {return rearLightIsOn;};
};

#endif /* LIGHTS_H_ */
