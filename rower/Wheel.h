/*
 * Wheel.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef WHEEL_H_
#define WHEEL_H_

class Wheel {
private:
	int frontWheelSize;
	int rearWheelSize;
public:
	Wheel();
	Wheel(int size);

	int getFrontWheelSize() const {return frontWheelSize;};
	int getRearWheelSize() const {return rearWheelSize;};

	void setFrontWheelSize(int size) {frontWheelSize = size;};
	void setRearWheelSize(int size) {rearWheelSize = size;};

};

#endif /* WHEEL_H_ */
