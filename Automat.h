/*
 * Automat.h
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef AUTOMAT_H_
#define AUTOMAT_H_
#include <string>

const int maxSize = 6;
const int maxCoinAmount = 10;


struct Products
{
	std::string name;
	int price;
	int limit;
};

class Automat {
public:
	Automat();
	virtual ~Automat();
	int showPrice(int index);
	int putMoney(int amount);
	int cancelPurchase();

private:
	Products listOfProducts[maxSize];
	Products choosenProduct;
	int givenMoney;
	int coinsLimit;


};

#endif /* AUTOMAT_H_ */
