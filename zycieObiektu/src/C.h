/*
 * C.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef C_H_
#define C_H_

#include "A.h"
#include "Z.h"

class C: virtual public A {
public:
	C();
	virtual ~C();
private:
	Z z;
};

#endif /* C_H_ */
