/*
 * B.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "B.h"
#include <iostream>

B::B(){
	std::cout << "Constructing B" << std::endl;
}

B::B(const B& other) {
	std::cout << "Copy constructor B" << std::endl;
}

B::~B(){
	std::cout << "Destroying B" << std::endl;
}
