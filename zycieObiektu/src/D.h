/*
 * D.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef D_H_
#define D_H_

#include "B.h"
#include "C.h"

class D: public B, public C {
public:
	D();
	virtual ~D();
};

#endif /* D_H_ */
