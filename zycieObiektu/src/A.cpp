/*
 * A.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "A.h"
#include <iostream>

A::A(){
	std::cout << "Constructing A" << std::endl;
	number=0;
}

A::~A(){
	std::cout <<"Destroying A" << std::endl;
}
