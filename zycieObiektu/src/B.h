/*
 * B.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef B_H_
#define B_H_

#include "A.h"
#include "Y.h"
#include "Z.h"

class B: virtual public A {
public:
	B();
	B(const B& other);
	~B();
private:
	Y y;
	Z z;

};

#endif /* B_H_ */
