//============================================================================
// Name        : zycieObiektu.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include <iostream>
#include "A.h"
#include "B.h"
#include "C.h"
#include "D.h"

using namespace std;

void f(A& someA){
cout<<someA.number<<endl;
someA.number=5;
cout<<someA.number<<endl;
}

int main() {
//	A tabA[3]={A(), A(), A()};
//	cout << tabA[0].number << endl;
//	cout << tabA[1].number << endl;
//	cout << tabA[2].number << endl;

	cout << "!!!Hello World!!!" << endl;
//	A a;
//	a.number=2;
//	f(a);
//	cout<<a.number<<endl;
	B b;
//	bc = b;
	B bb(b);

//	cout << "Smoething more complicated"<< endl;
//
//	A *a= new D;
//	delete a;

	cout << "!!!Exiting the program!!!" << endl;
	return 0;
}
