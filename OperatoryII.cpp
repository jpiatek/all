//============================================================================
// Name        : OperatoryII.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void sizeTypes(){
		cout<<"size of bool: "<<sizeof(bool) << endl;
		cout<<"size of char: "<<sizeof(char) << endl;
		cout<<"size of int: "<<sizeof(int) << endl;
		cout<<"size of double: "<<sizeof(double)<< endl;
	}



class MyClass{
private:
	int counter;
public:
	static void Foo2(){
		cout << "Foo2" <<endl;
	}
	void Foo(){
		Foo2();
		cout<<"Foo"<< endl;
	}
	void Foo() const{
		Foo2();
		cout << "Foo const" << endl;
	}

};

int main() {

	sizeTypes();

	int x =5;
	int y=6;
	int *p;
	p = &x;
	p = &y;
	*p = 10;

	if(y == 10)
		cout <<"y == 10";
	else
		cout<<"y!=10";


//	int n1=0, n2=3;
//	float n3=3,n4=6;
//	n1=(n3=(n2=5)*4/8.0)*2;
//	cout << n1 << endl << n2 <<endl << n3 << endl;
	return 0;
}
