
#ifndef DATE_H_
#define DATE_H_

class Date{

public:
	Date();
	Date (int nday, int nmonth, int nyear);
	int getDay()const;
	void setDay(int day);
	int getMonth()const;
	void setMonth(int month);
	int getYear()const;
	void setYear(int year);
	bool isValid()const;
	void printDate()const;
	bool isInLappYear()const;
	int daysTillTheEndOfTheMonth()const;
	int daysTillTheEndOfTheYear()const;
	bool operator==(const Date& other)const;
	bool operator!=(const Date& other)const;
	bool operator>=(const Date& other)const;

private:
		int day;
		int month;
		int year;
};


#endif /* DATE_H_ */
