//============================================================================
// Name        : 16_03_3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

const int max_size=5;

int main() {

	int t[max_size]={4, 3, 6, 7, 1};
	int *it=t;
	int suma=0;

		for (int i=0; i<max_size; ++i){

			suma += *it;
			++it;

			}
	cout << "suma to: " << suma << endl;

	return 0;
}
