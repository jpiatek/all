/*
 * LogWriter.h
 *
 *  Created on: 12.04.2017
 *      Author: RENT
 */

#ifndef LOGWRITER_H_
#define LOGWRITER_H_
#include <fstream>
#include <string>
#include <iostream>

class LogWriter {
public:
	LogWriter();
	virtual ~LogWriter();
	enum LogType{Fatal, Error, Warning, Info, Debug, Trace};
	void logMessage(std::string message, LogType messageType);
	void setLoggingLevel(LogType logLevel);

private:
	std::fstream f;
	LogType logLevel;
};

#endif /* LOGWRITER_H_ */
