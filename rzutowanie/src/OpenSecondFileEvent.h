/*
 * OpenSecondFileEvent.h
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef OPENSECONDFILEEVENT_H_
#define OPENSECONDFILEEVENT_H_

#include "Event.h"

class OpenSecondFileEvent: public Event {
public:
	OpenSecondFileEvent(std::string fileToOpen);
	virtual ~OpenSecondFileEvent();
	std::string getEventType();
	std::string getFileName();
private:
	static const  std::string eventType;
	std::string fileName;
};

#endif /* OPENSECONDFILEEVENT_H_ */
