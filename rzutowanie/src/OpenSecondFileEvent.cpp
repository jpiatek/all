/*
 * OpenSecondFileEvent.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "OpenSecondFileEvent.h"

const std::string OpenSecondFileEvent::eventType = "OpenSecondFileEvent";

OpenSecondFileEvent::OpenSecondFileEvent(std::string fileToOpen):fileName(fileToOpen) {

}

OpenSecondFileEvent::~OpenSecondFileEvent() {
}

std::string OpenSecondFileEvent::getEventType()
{
	return eventType;
}
std::string OpenSecondFileEvent::getFileName()
{
	return fileName;
}
