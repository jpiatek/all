/*
 * OpenFileEvent.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "OpenFileEvent.h"

const std::string OpenFileEvent::eventType = "OpenFileEvent";

OpenFileEvent::OpenFileEvent(std::string fileToOpen):fileName(fileToOpen) {

}

OpenFileEvent::~OpenFileEvent() {
}

std::string OpenFileEvent::getEventType()
{
	return eventType;
}
std::string OpenFileEvent::getFileName()
{
	return fileName;
}
