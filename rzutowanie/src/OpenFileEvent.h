/*
 * OpenFileEvent.h
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef OPENFILEEVENT_H_
#define OPENFILEEVENT_H_

#include "Event.h"

class OpenFileEvent: public Event {
public:
	OpenFileEvent(std::string fileToOpen);
	virtual ~OpenFileEvent();
	std::string getEventType();
	std::string getFileName();
private:
	static const std::string eventType;
	std::string fileName;
};

#endif /* OPENFILEEVENT_H_ */
