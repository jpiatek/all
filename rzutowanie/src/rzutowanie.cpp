//============================================================================
// Name        : rzutowanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Event.h"
#include "OpenFileEvent.h"
#include "OpenSecondFileEvent.h"
using namespace std;

void displayEvent(Event *event){
	cout<< "Received: "<<event->getEventType() << " ";
	if(event->getEventType() == "OpenFileEvent")
	{
		OpenFileEvent *openEvent = static_cast<OpenFileEvent*>(event);
		cout<<"Trying to open file: "<<openEvent->getFileName();
	}
	if(event->getEventType() == "OpenSecondFileEvent")
	{
		OpenSecondFileEvent *openSecond = static_cast<OpenSecondFileEvent*>(event);
		cout<<"Trying to open file: "<<openSecond->getFileName();
	}
	cout<< "\n";
}

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!


	Event gev = Event();
	OpenFileEvent oev("a.txt");
	OpenSecondFileEvent ose("b.txt");

	Event *ev = &gev;
	displayEvent(ev);
	ev = &oev;
	displayEvent(ev);
	ev = &ose;
	displayEvent(ev);

	return 0;
}
