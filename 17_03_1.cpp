//============================================================================
// Name        : 17_03_1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <iomanip>
using namespace std;

const int max_size=15;

int main() {

	int t[max_size]={55, 3, 4, 52, 45, 66, 73, 21, 7, 53, 62, 0, 12, 32, 76};
	int pamiec;
	int i;


	cout << "przed sortowaniem" << endl;

	for(int i=0; i < max_size; ++i){
		cout << t[i] << " ";
	}
	cout << endl;

	for(int j=max_size-2; j>=0;--j){
		pamiec=t[j];
		i = j+1;
		while((i<max_size) && (pamiec>t[i])){
			t[i-1]=t[i];
			++i;
		}
		t[i-1]=pamiec;
	}

	cout << "sortowanie przez wstawianie" << endl;

	for(i=0; i<max_size; ++i){
		cout << t[i]<< " ";
	}

	return 0;
}
