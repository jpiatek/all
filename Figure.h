/*
 * Figure.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef FIGURE_H_
#define FIGURE_H_
#include <string>

class Figure {

public:
	Figure(std::string givenName);

	virtual double area()=0;
	virtual void draw()=0;
	virtual void getDescription()=0;
	std::string getName();

private:
	std::string name;

};

#endif /* FIGURE_H_ */
