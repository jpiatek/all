/*
 * LogWriter.cpp
 *
 *  Created on: 12.04.2017
 *      Author: RENT
 */

#include "LogWriter.h"
#include <ctime>

LogWriter::LogWriter() {
	f.open("logi.txt", std::ios_base::out);
	logLevel = Error;

}

LogWriter::~LogWriter() {
	f.close();
}

void LogWriter::logMessage(std::string message, LogType messageType){
	if(messageType<=logLevel){

		time_t now = time(0);
		char* dt = ctime(&now);
		f << message << " - " << dt;
		std::cout << message << " - " << dt << std::endl;
	}
}
