//============================================================================
// Name        : sortowanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <ctime>
#include <iomanip>
using namespace std;

void insertion_sort(int *tab, int n){
		int temp, j;

		for(int i=1;i<n;i++){
			temp = tab[i];
			j = i -1;
			while(j>0 && tab[j]>temp){
				tab[j+1]=tab[j];
				--j;
			}
			tab[j+1]=temp;
		}
	}

void quick_sort(int *tab, int left, int right){
	if(right<=left){
		return;
	}
	int i = left - 1;
	int j = right + 1;
	int pivot = tab[(left+right)/2];

	while(true){
		while(pivot>tab[++i]);
		while(pivot<tab[--j]);
		if(i<=j)
			swap(tab[i],tab[j]);
		else
			break;
	}
	if(j>left)
		quick_sort(tab, left, j);
	if(i<right)
		quick_sort(tab, i, right);
}

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
clock_t tStart = clock();
int n = 5;
int tab[n] = {3,21,12,87,45};
cout <<"tablica przed sortowaniem:\n";
for(int i=0;i<n;i++){
	cout<<tab[i]<<" ";
}
cout<<endl;


//insertion_sort(tab, n);
quick_sort(tab,0,n-1);

cout<<"Posortowana tablica:\n";
for(int i=0;i<n;i++){
	cout<<tab[i]<<" ";
}

cout<<"Time taken: "<<fixed<<setprecision(3)<< (1.0*clock() - tStart)/CLOCKS_PER_SEC<<"\n";
	return 0;
}
