//============================================================================
// Name        : binarne.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int solution(int A, int B){
	unsigned long long int A2=A;
	unsigned long long int result = A2*B;
	int counter = 0;

	while (result) {
		if (result % 2 == 1) {
			counter++;
		}
		result /= 2;
	}
	return counter;
}

int main() {
	cout<<solution(100000000,100000000);
	return 0;
}
