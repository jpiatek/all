/*
 * Wheel.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Wheel.h"
#include <iostream>
#include <cmath>

Wheel::Wheel(std::string givenName, double nr):
Figure(givenName){
	r=nr;
}
double Wheel::area(){

	return 3.14*(r*r);

}
void Wheel::draw(){


	for(double y=0; y<(r*2); ++y){
		for(double x=0; x<(r*2); ++x){

			if( sqrt((r-y)*(r-y) + (r-x)*(r-x)) <r){
				std::cout << "* ";
			}
			else{
				std::cout << "  ";
			}
		}
		std::cout<<std::endl;
	}
}
void Wheel::getDescription(){
 std::cout << getName() << std::endl;
 std::cout << "promien wynosi: " << r <<std::endl;
 std::cout << "pole wynosi: " << area() <<std::endl;
 draw();

}
